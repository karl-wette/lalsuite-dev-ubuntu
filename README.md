LALSuite Development - Ubuntu Docker Image
==========================================

Provides a Docker image for building and testing LALSuite on Ubuntu.

Updates to the Docker image should be submitted as merge requests.
