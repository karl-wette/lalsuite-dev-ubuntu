# syntax=docker/dockerfile:1

FROM ubuntu:noble

LABEL name="LALSuite Development - Ubuntu Noble" \
      maintainer="Karl Wette <karl.wette@ligo.org>" \
      support="Not Supported"

ARG DEBIAN_FRONTEND="noninteractive"
ENV TZ=Etc/UTC
RUN <<EOF
apt-get -y update
apt-get -y upgrade
apt-get -y install \
    autoconf \
    automake \
    build-essential \
    git \
    make \
    software-properties-common \
    ;
add-apt-repository -y ppa:kwwette/gwda
apt-get -y update
apt-get -y install \
    bc \
    ccache \
    debhelper \
    dh-python \
    help2man \
    ldas-tools-framecpp-c-dev \
    less \
    libcfitsio-dev \
    libfftw3-dev \
    libframel-dev \
    libgsl-dev \
    libhdf5-dev \
    libmetaio-dev \
    liboctave-dev \
    libopenmpi-dev \
    pkg-config \
    python3-all-dev \
    python3-astropy \
    python3-dateutil \
    python3-freezegun \
    python3-h5py \
    python3-healpy \
    python3-igwn-ligolw \
    python3-igwn-segments \
    python3-ligo-lw \
    python3-ligo-segments \
    python3-lscsoft-glue \
    python3-matplotlib \
    python3-mpmath \
    python3-numpy \
    python3-pil \
    python3-pytest \
    python3-scipy \
    python3-six \
    python3-tqdm \
    rsync \
    swig \
    zlib1g-dev \
    ;
EOF
